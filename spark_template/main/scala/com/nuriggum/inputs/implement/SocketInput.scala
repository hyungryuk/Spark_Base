package com.nuriggum.inputs.implement

import com.nuriggum.inputs.`trait`.InputTrait
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  *
  * @param _options filePath
  */
class SocketInput(val _options:Map[String,String]) extends InputTrait{

  override val options: Map[String, String] = _options

  override def createStructuredStreaming(): DataFrame = {

    val spark = SparkSession.builder().getOrCreate()

    spark.readStream
      .format("socket")
      .option("host", options.get("socket.host").get)
      .option("port", options.get("socket.port").get)
      .load()
  }
}
